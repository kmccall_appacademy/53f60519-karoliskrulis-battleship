class Board

  attr_reader :grid, :marks
  def initialize(grid = Board.default_grid)
    @grid = grid
    @marks = :s, :x
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def count
    num = 0
    grid.each do |g|
      num += g.count(:s)
    end
    num
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    grid.flatten.none? { |pos| pos.nil? }
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, value)
    row, col = pos
    grid[row][col] = value
  end

  # def display
  #   top = [*0..grid.size].join
  #   puts top
  # end

  def place_random_ship
    raise "board is full" if full?
    l = grid.length
    pos = [rand(l), rand(l)]

    # find a spot that isn't taken
    until empty?(pos)
      pos = [rand(l), rand(l)]
    end
    self[pos] = :s
  end

  def won?
    grid.flatten.none? { |s| s == :s }
  end

end
